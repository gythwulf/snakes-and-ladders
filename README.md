# Snakes And Ladders  :snake:
Copyright (C) 2018 Daniel Wheeler and Taniya Nanda

A basic snakes and ladder written for a CS 410P Rust Programming course at Portland State University

# Getting Started

Installation

git clone https://gitlab.com/gythwulf/snakes-and-ladders.git

Usage 

Using a command line tool, navigate to the project directory and execute the command: cargo run 

# How To Play

https://www.wikihow.com/Play-Snakes-and-Ladders

# Licensing

This program is licensed under the "MIT License". Please see the file LICENSE in the source distribution of this software for license terms.

# Project Post Mortem

The end result of this project did not turn out exactly as was envision. We invisioned showing players on the board and displaying the board, snakes, and ladders for each player turn, but we ran short on time. It also would have been nice to make the random number generator used for the Dice class more random than it is. The game otherwise plays exactly as intended. Also I have to say that for as much as we wrestled with the borrow checker, it wasn't as bad as we were expecting.
