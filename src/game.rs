/*
 Copyright © 2018 Daniel Wheeler and Taniya Nanda
 [This program is licensed under the "MIT License"]
 Please see the file LICENSE in the source
 distribution of this software for license terms.
*/

extern crate rand;

use rand::Rng;
use std::collections::HashMap;
use std::io::{self};

/*
 * The Game class, which includes players, the board,
 * snakes, ladders, and dice.
*/
pub struct Game {
    players: Vec<Player>,
    board: GameBoard,
    snakes: HashMap<u8, u8>,
    ladders: HashMap<u8, u8>,
    dice: Dice,
}

impl Game {
    // Create a new Game object. Note that only the players are
    // dynamic - The board, snakes, ladders and dice are
    // pre-defined.
    pub fn new(players: Vec<String>) -> Game {
        let mut p = Vec::new();

        for player in players {
            p.push(Player::new(player));
        }

        Game {
            players: p,
            board: GameBoard::new(),
            snakes: [
                (43, 16),
                (55, 34),
                (70, 48),
                (78, 42),
                (95, 73),
                (96, 82)
            ].iter()
                .cloned()
                .collect(),
            ladders: [
                (6, 27),
                (9, 50),
                (20, 39),
                (25, 57),
                (53, 72),
                (54, 85),
                (61, 82),
            ].iter()
                .cloned()
                .collect(),
            dice: Dice::new(6),
        }
    }

    // Play the game!
    pub fn play(&mut self) {
        // First let's display the board, players, snakes, and ladders.
        self.board.display();
        self.display_players();
        self.display_ladders();
        self.display_snakes();

        // Loop until we hit the win condition.
        'game: loop {
            for player in &mut self.players {
                // Let's make it clear when we display information of the
                // next player.
                println!("==============================================");
                println!("It is {}'s turn. You are at position {}.",
                         player.get_name(), player.get_loc());

                // While the player has rolled a 6, they get to go again
                while {
                    // Kind of hacky, since we don't actually use the input,
                    // but wait for player input before continuing so it
                    // feels like they are doing something.
                    let mut input = String::new();
                    println!("Hit any key when you are ready to roll.");
                    io::stdin().read_line(&mut input);

                    // Get the player's location, roll the dice, and
                    // get the new location.
                    let mut loc = player.get_loc();
                    let mut roll = self.dice.roll();
                    let mut new_loc = loc + roll;

                    println!("You rolled a {}!", roll);

                    // If a player reaches 100, they win!
                    if new_loc == 100 {
                        println!("{} reached the end and won the game!",
                                 player.get_name());
                        break 'game;
                    }

                    // You must land on 100 to win.
                    if new_loc > 100 {
                        println!("You rolled too high! You need a roll of exactly {} to finish the game.", 100 - loc);
                    }

                    // If they roll a 6, they get to go again, so we should
                    // let them know.
                    if roll == 6 {
                        println!("You rolled a {} and get to go again!!", roll);
                    }

                    // We don't want to go through this bit if the player rolled
                    // too high.
                    if new_loc < 100 {
                        println!("You progress from {} to {}.", loc, new_loc);

                        // Did the player land on a snake?
                        match self.snakes.get(&new_loc) {
                            Some(x) => {
                                println!("You hit a snake and lost {} spaces!",
                                         new_loc - x);
                                new_loc = *x;
                                println!("You are now on {}.", new_loc);
                            }
                            None => (),
                        }

                        // Did the land on a ladder?
                        match self.ladders.get(&new_loc) {
                            Some(x) => {
                                println!("You hit a ladder and gained {} spaces!",
                                         x - new_loc);
                                new_loc = *x;
                                println!("You are now on {}.", new_loc);
                            }
                            None => (),
                        }

                        // Update the player's location
                        player.set_loc(new_loc);
                    }

                    // If they didn't roll a 6, get player input to
                    // end their turn.
                    if roll != 6 {
                        println!("Hit any key to end your turn.");
                        io::stdin().read_line(&mut input);
                    }

                    // Loop while roll == 6
                    roll == 6
                } {}

                // Separate the output a bit to make it easier to read
                println!("");
            }
        }
    }

    // Display the ladders in the form start -> finish
    fn display_ladders(&self) {
        println!("Ladders:");
        for (key, val) in &self.ladders {
            println!("{0: >2} -> {1: >2}", key, val)
        }
        println!("");
    }

    // Display the snakes in the form start -> finish
    fn display_snakes(&self) {
        println!("Snakes:");
        for (key, val) in &self.snakes {
            println!("{0: >2} -> {1: >2}", key, val)
        }
        println!("");
    }

    // Display each player. See Player.display() for formatting.
    fn display_players(&self) {
        println!("Players:");
        for player in &self.players {
            player.display()
        }
        println!("");
    }
}

/*
 * Struct of the game board 
 * that has a member of board in it. 
 * this struct can be used to build the board.
*/
struct GameBoard {
    board: Vec<Vec<u8>>,
}

impl GameBoard {
    fn new() -> GameBoard {
	// first row of the board from [1-10]
        let first_row = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

	// second row of the board from [11 - 20]
        let second_row = vec![20, 19, 18, 17, 16, 15, 14, 13, 12, 11];

	//third row of the board from [21 - 30]
        let third_row = vec![30, 29, 28, 27, 26, 25, 24, 23, 22, 21];

	// fourth row of the board
        let fourth_row = vec![40, 39, 38, 37, 36, 35, 34, 33, 32, 31];

	// fifth row of the board
        let fifth_row = vec![50, 49, 48, 47, 46, 45, 44, 43, 42, 41];

	// sixth row of the board
        let sixth_row = vec![60, 59, 58, 57, 56, 55, 54, 53, 52, 51];

	// seventh row of the board
        let seventh_row = vec![70, 69, 68, 67, 66, 65, 64, 63, 62, 61];

	//Eigth row of the board
        let eigth_row = vec![80, 79, 78, 77, 76, 75, 74, 73, 72, 71];

	//Ninth row of the board
        let ninth_row = vec![90, 89, 88, 87, 86, 85, 84, 83, 82, 81];

	// Tenth row of the board
        let tenth_row = vec![100, 99, 98, 97, 96, 95, 94, 93, 92, 91];

	/*
		This create the board by passing in the 
		vector and the rows in order into the vector.
	*/
        GameBoard {
            board: vec![
                tenth_row,
                ninth_row,
                eigth_row,
                seventh_row,
                sixth_row,
                fifth_row,
                fourth_row,
                third_row,
                second_row,
                first_row,
            ],
        }
    }

    /*
	This function print the board 				
    */
    fn display(&self) {
	
        let separator = "+---+---+---+---+---+---+---+---+---+---+";

	/*
		Iterate the vector and print each vector
		inside it. The for loop prints the vectors 
	*/
        for row in &self.board {
            println!("{}", separator);

            for i in row.iter() {
                print!("|{0: >3}", i)
            }
	/*
		"|" between each number in the board
	*/
            println!("|")
        }

        println!("{}", separator);

        println!("");
    }
  }

/*
	This struct is for each player in the game
	It holds the name and location of the player
	name: name of the player
	loc: location of the player on the board
*/
struct Player {
    name: String,
    loc: u8,
}
/*
	Implementation of the player struct 
	include constructor, set location function,
	get location function, set location function, get name and display function

*/
impl Player {
    /*
	Sets the name as what's passed in the function 
        and location as 1
     */
    fn new(n: String) -> Player {
        Player { name: n, loc: 1 } 
    }

    /*
	sets the location to 1
    */
    fn set_loc(&mut self, l: u8) {
        self.loc = l
    }

    /*
	return the location of the player
    */
    fn get_loc(&self) -> u8 {
        self.loc
    }
    /*
	return the name of the player
    */
    fn get_name(&self) -> &str {
        self.name.as_str()
    }

    /*
	This function return the name and position of the player
    */
    fn display(&self) {
        println!("Name: {}, position: {}", self.name, self.loc);
    }
}

// We assume the dice has a range of 1-max
pub struct Dice {
    max: u8,
}

impl Dice {
    // On instantiation we set the max value.
    pub fn new(max: u8) -> Dice {
        Dice { max: max }
    }

    // Roll the dice! We must add 1 to the roll,
    // otherwise it would be 0-(max-1)
    pub fn roll(&mut self) -> u8 {
        let mut rng = rand::thread_rng();

        rng.gen::<u8>() % self.max + 1
    }
}

/*
 * Only tests follow
*/

#[test]
fn test_player_name() {
    let p = Player::new(String::from("tanya"));

    assert_eq!(p.get_name(), "tanya");
}

#[test]
fn test_player_loc() {
    let mut p = Player::new(String::from("tanya"));
    let n: u8 = 4;

    p.set_loc(n);

    assert_eq!(p.get_loc(), n);

    p.set_loc(n+1);

    assert_eq!(p.get_loc(), n+1);
}

#[test]
fn test_player_display() {
    let p = Player::new(String::from("tanya"));

    p.display();
}

#[test]
fn test_dice() {
    let mut d: Dice = Dice::new(6);

    // I figure in 100 rolls I have a pretty good chance of rolling
    // every number at least once.
    for i in 1 .. 100 {
        let r = d.roll();

        assert!(r > 0 && r < 7);
    }
}

#[test]
fn test_board() {
    let check = GameBoard::new();
    check.display();
}

#[test]
fn test_game_display() {
    let mut p = Vec::new();
    p.push(String::from("t"));

    let g = Game::new(p);
    g.display_ladders();
    g.display_snakes();
    g.display_players();
}
