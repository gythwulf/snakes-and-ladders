/*
 Copyright © 2018 Daniel Wheeler and Taniya Nanda
 [This program is licensed under the "MIT License"]
 Please see the file LICENSE in the source
 distribution of this software for license terms.
*/

extern crate game;

use game::Game;

use std::io::{self, BufRead};

fn main() {
    let mut input = String::new();
    let stdin = io::stdin();

    // Figure out how many players to populate
    println!("How many players are there (2-4)?");
    stdin.lock().read_line(&mut input).unwrap();

    // Hopefully they provide an integer, but yell at them
    // just in case they didn't.
    let num_players = input.trim_right().parse::<u8>().expect(
        "You must specify an integer.",
    );

    // Yell at them again if they didn't provide a number
    // in range.
    if num_players < 2 || num_players > 4 {
        println!("Only 2-4 players are supported.");
        std::process::exit(1);
    }

    let mut players: Vec<String> = Vec::new();

    // Gather the names of the players
    for i in 0..num_players {
        let mut input = String::new();

        println!("Name of player {}:", i + 1);

        stdin.lock().read_line(&mut input).expect(
            "Could not read input name.",
        );

        // Make sure there is no newline character in the name.
        let player = input.trim();

        // Empty names are not allowed!
        if player.len() == 0 {
            println!("You cannot have an empty string for a player name!");
            std::process::exit(1);
        }

        players.push(player.to_string());
    }

    // Set up the game!
    let mut game = Game::new(players);

    // Time to play!
    game.play()
}
